#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal
import os
from glob import glob
from scipy.ndimage.filters import gaussian_filter
import sys

import config

def save_plt(path, remove_axis=False):
  """ Saves the current figure. """
  if not os.path.isdir(os.path.dirname(path)):
    os.makedirs(os.path.dirname(path))
  plt.tight_layout()
  if remove_axis:
    plt.axis('off')
  plt.savefig(path, dpi=300)


def read_bin(path):
  """Reads the raw input image.

  Args:
    path (str): path to the the binary input image.

  Returns:
    np.array: The image as 2D numpy float32 array.
  """
  image = np.fromfile(path,dtype=np.uint16)     # Load the data
  image = image.astype(np.float32)              # Convert to float32
  image = image / np.iinfo(np.uint16).max
  np.iinfo(np.uint16).max# Scale the data
  image = image.reshape(config.image_rows, config.image_cols)   # Reshape the data
  return image


def demosaic(im):
  """ Computes a demosaic version of the input image.

  The input image has only one channel, but the color information is encoded
  as a Bayer mosaic:
  R G
  G B
  The function interpolates the missing values.

  Args:
    im: a float32 numpy array with shape (H,W,1).

  Returns:
    np.array: The demosaiced image with shape (H,W,3).
  """
  # create an empty image with 3 channels
  img = np.zeros((config.image_rows, config.image_cols, 3))

  rows = int(config.image_rows / 2)
  cols = int(config.image_cols / 2)
  # create a mask to get only the red, green or blue pixels accordingto the Bayer mosaic used
  red_mask = np.matlib.repmat([[1, 0], [0, 0]], rows, cols)
  green_mask = np.matlib.repmat([[0, 1], [1, 0]], rows, cols)
  blue_mask = np.matlib.repmat([[0, 0], [0, 1]], rows, cols)

  # create a constant filter
  F = [[1, 1, 1], [1, 1, 1], [1, 1, 1]]

  # apply the mask to the image,to separate the red, green and blue pixels in the right channel
  # convolve the different channel to interpolate the missing values (border handling method: zero)
  img[:, :, 0] = scipy.signal.correlate2d(im * red_mask, F, mode='same', boundary="symm")
  img[:, :, 1] = scipy.signal.correlate2d(im * green_mask, F, mode='same', boundary="symm")
  img[:, :, 2] = scipy.signal.correlate2d(im * blue_mask, F, mode='same', boundary="symm")

  # create the filter to normalize
  weights_red_image = np.matlib.repmat([[1, 0.5], [0.5, 0.25]], rows, cols)
  weights_green_image = np.matlib.repmat([[0.25, 0.2], [0.2, 0.25]], rows, cols)
  weights_blue_image = np.matlib.repmat([[0.25, 0.5], [0.5, 1]], rows, cols)

  # apply the normalization
  img[:, :, 0] *= weights_red_image
  img[:, :, 1] *= weights_green_image
  img[:, :, 2] *= weights_blue_image

  return img


def white_roi(im):
  list=[]
  for x in range(0,config.image_rows-9,5):
    for y in range(0,config.image_cols-9,5):
      region = im[x:x + 10, y:y + 10]
      avg=np.mean(region)
      if (avg>0.95):
        list.append([x,y])

  return list

def find_gray(im):
  list = []
  for x in range(0, config.image_rows):
    for y in range(0, config.image_cols):
      r,g,b=im[x, y]
      if (r==g==b!=0):
        list.append([x,y])





def white_balancing_params(im,img):

  """ Computes the parameters needed for the white white balancing.

  Args:
    im: a (H,W,3) numpy float32 array.

  Returns:
    np.array: a RGB offsets that are added to the image for white balancing.
  """
  r_offset=[]
  g_offset = []
  b_offset = []

  regions=white_roi(im)

  for [x,y] in regions:

    r=img[x:x+9,y:y+9,0]
    g=img[x:x + 9, y:y + 9, 1]
    b=img[x:x+9,y:y+9,2]
    average=(np.mean(r)+np.mean(g)+np.mean(b))/3

    r_offset.append(average-np.mean(r))
    g_offset.append(average - np.mean(g))
    b_offset.append(average - np.mean(b))

  return np.array((np.mean(r_offset),np.mean(g_offset),np.mean(b_offset)))




def white_balancing(im, params):
  """ Performs white balancing using the provided parameters.

  Args:
    im: a (H,W,3) numpy float32 array.
    params: The white balancing parameters as computed by white_balancing_params.

  Returns:
    A white balanced version of the input image.

  """
  whitened=im+params
  im=np.clip(whitened,0,1)

  return im

def gamma_correction(im, gamma=0.85, upper_limit=0.3):
    """ Gamma Correction of the input image.
    \begin{align}
    T &= I^\gamma * \tfrac{0.8}{l_u} \\
    O &= \clip(T, 0, 0.8) + 0.2 \tfrac{\clip(T, 0.8, 4.8) - 0.8}{4}
    \end{align}
    Args:
    im: input image I
    gamma: gamma
    upper_limit: u_l
    Returns:
    A numpy float32 array
    """
    T = 0.8*(im**gamma*upper_limit**-1)
    return np.clip(T, 0, 0.8)+0.2*(np.clip(T,0.8, 4.8)-0.8 / 4.0)

def sharpening(im):
  """ Sharpening filter.

  Computes $2I - G*I$

  Args:
    im: Input image I.

  Returns:
    A numpy float32 array.
"""

  G=gaussian_filter(im, sigma=(13,13,1),mode='nearest')#sigma(m,n,step)
  O=2*im-G
  print("test")
  return O

def mapping(im): #                                                               `      TODO: find good parameters
  """
  Maps the values of im to the range of [0,1] using the lower percentile
  and upper percentile.

  Args:
    im: input image.
    lower_p: lower percentile.
    upper_p: upper percentile.

  Returns:
    A numpy float32 array.
  """

  low = np.clip(np.percentile(im, 0.01),0,1)
  high = np.clip(np.percentile(im, 99.8),0,1)

  im = (im - low) / (high - low)
  im = np.clip(im, 0, 1)
  return im


def main():
  bin_paths = glob(os.path.join('../data', '*.bin'))
  bin_paths.sort()

  wb_params = None
  for bin_path_idx, bin_path in enumerate(bin_paths):
    print('[INFO] process im %s' % bin_path)
    im = read_bin(bin_path)
    plt.figure()
    plt.imshow(mapping(im), cmap='gray')
    save_plt('../doc/gfx/00_raw_%02d.png' % bin_path_idx, remove_axis=True)

    img = demosaic(im)
    plt.figure()
    plt.imshow(mapping(img))
    save_plt('../doc/gfx/01_demosaiced_%02d.png' % bin_path_idx, remove_axis=True)
    #find_gray(img)
    if wb_params is None:
      params=white_balancing_params(im,img)
    white=white_balancing(img, params)
    plt.figure()
    plt.imshow(mapping(white))
    save_plt('../doc/gfx/02_white_balanced_%02d.png' % bin_path_idx, remove_axis=True)

    gamma = gamma_correction(white)
    plt.figure()
    plt.imshow(mapping(gamma))
    save_plt('../doc/gfx/03_gamma_corrected_%02d.png' % bin_path_idx, remove_axis=True)

    sharp = sharpening(gamma)
    plt.figure()
    plt.imshow(mapping(sharp))
    save_plt('../doc/gfx/04_sharped_%02d.png' % bin_path_idx, remove_axis=True)

  #plt.show()

if __name__ == "__main__":
  main()