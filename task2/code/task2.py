#!/usr/bin/env python

import numpy as np
import matplotlib
matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import scipy.signal
import skimage.transform
import os
from glob import glob
import sys
import json
import time

sys.path.append('/home/ivona//anaconda3/lib/python3.5/site-packages')
#import cv2


import config

sys.path.append('native')
import pynative

def save_plt(path, remove_axis=False):
  """ Saves the current figure. """
  plt.tight_layout()
  if remove_axis:
    plt.axis('off')
  plt.savefig(path, dpi=300)


def read_bin(path):
  """Reads the raw input image.

  Args:
    path (str): path to the the binary input image.

  Returns:
    np.array: The image as 2D numpy float32 array.
  """

  im = np.fromfile(path, dtype='uint16').reshape(config.image_rows, config.image_cols)
  return im.astype(np.float32) / 2**16


def demosaic(im):
  """ Computes a demosaic version of the input image.

  The input image has only one channel, but the color information is encoded
  as a Bayer mosaic:
  R G
  G B
  The function interpolates the missing values.

  Args:
    im: a float32 numpy array with shape (H,W,1).

  Returns:
    np.array: The demosaiced image with shape (H,W,3).
  """

  im3 = np.zeros((im.shape[0], im.shape[1], 3), dtype=im.dtype)
  kernel = np.array([
    [[0.25, 0.5, 0.25],[0.5, 1.0, 0.5],[0.25, 0.5, 0.25]],
    [[0.0, 0.25, 0.0],[0.25, 1.0, 0.25],[0.0, 0.25, 0.0]],
    [[0.25, 0.5, 0.25],[0.5, 1.0, 0.5],[0.25, 0.5, 0.25]]])
  mask = np.array([[0,1],[1,2]])
  mask = np.tile(mask, (int(im.shape[0]/2), int(im.shape[1]/2)))
  for i in range(3):
      tmp = im.copy()
      tmp[mask != i] = 0
      # im3[:,:,i] = cv2.filter2D(tmp,-1,kernel[i,:,:])
      im3[...,i] = scipy.signal.correlate2d(tmp, kernel[i,:,:], mode='same', boundary='wrap')
  return im3


def mapping(im, lower_p=0.1, upper_p=99.8):
  """
  Maps the values of im to the range of [0,1] using the lower percentile
  and upper percentile.

  Args:
    im: input image.
    lower_p: lower percentile.
    upper_p: upper percentile.

  Returns:
    A numpy float32 array.
  """
  mapped = np.empty_like(im)
  if len(im.shape) == 2:
    mi = np.percentile(im, lower_p)
    ma = np.percentile(im, upper_p)
    mapped = np.clip((im - mi) / (ma - mi), 0, 1)
  for ch in range(3):
    mi = np.percentile(im[...,ch], lower_p)
    ma = np.percentile(im[...,ch], upper_p)
    mapped[...,ch] = np.clip((im[...,ch] - mi) / (ma - mi), 0, 1)
  return mapped


def preprocess(bin_path):
  im = read_bin(bin_path)
  im = demosaic(im)
  return im

def interpolate_nn(im, step, orig_shape):
  im = im[::step, ::step]
  im = im.repeat(step, axis=0).repeat(step, axis=1)
  im = im[:orig_shape[0], :orig_shape[1]]
  return im

def init_undistort(dist_coeffs, focal_length, principal_point, im_size):
  # TODO use the distortion coefficients and intrinsic parameters to compute
  # an inverse map for distortion.

  [k1,k2,p1,p2,k3]=dist_coeffs

  # 1 step: (Uu, Vu)
  u = range(0,im_size[0])
  v = range(0,im_size[1])
  v_u, u_u = np.meshgrid(u,v)
  
  # 2 step: (Xu,Yu)
#  K = np.array([[focal_length,  0,            principal_point[0]],
#                [0,             focal_length, principal_point[1]],
#                [0,             0,            1                 ]])
  x_u = (u_u - principal_point[1]) / focal_length[1]
  y_u = (v_u - principal_point[0]) / focal_length[0]
              
  # 3 step: (Xd,Yd)
  r = np.sqrt(pow(x_u,2)+pow(y_u,2)) 
  # warning if error it because of (i,j) = (y,x)
  x_d = x_u * (1+k1*pow(r,2)+k2*pow(r,4)+k3*pow(r,6)) + 2*p1*x_u*y_u + p2*(pow(r,2)+2*pow(x_u,2))
  y_d = y_u * (1+k1*pow(r,2)+k2*pow(r,4)+k3*pow(r,6)) + p1*(pow(r,2)+2*pow(y_u,2)) +2*p2*x_u*y_u

  # 4 step: (Ud,Vd)
  u_d = focal_length[1] * x_d + principal_point[1]
  v_d = focal_length[0] * y_d + principal_point[0]

  return np.array([u_d,v_d])

def undistort(im, map):
  # TODO: use the inverse transformation map to perform image undistortion
  im[:,:,0] = skimage.transform.warp(im[:,:,0],map,order=3)
  im[:,:,1] = skimage.transform.warp(im[:,:,1],map,order=3)
  im[:,:,2] = skimage.transform.warp(im[:,:,2],map,order=3)
  
  return im

def rectify(im, H):
  # TODO use the rectification homography to rectify the image
  im[:,:,0] = skimage.transform.warp(im[:,:,0],H,order=3)
  im[:,:,1] = skimage.transform.warp(im[:,:,1],H,order=3)
  im[:,:,2] = skimage.transform.warp(im[:,:,2],H,order=3)
  return im



def postprocess_disparity(dispr, displ, costr):
  # TODO: use the right and left disparity map for a consistency check in the
  # right map.
  # Then use the associated matching costs to discard disparity values with
  # too large costs.
  # Finally, remove disparity values that are outside the valid range.

  cost_thresh = np.mean(costr)
  costr[costr>cost_thresh]=0

  dispr_thresh = (np.mean(dispr) + np.mean(displ)) / 2.0
  lrdif = np.abs(displ - dispr)
  dispr[lrdif>dispr_thresh]=0

  return dispr,costr


def depth_from_disparity(disp, T, focal_length):
  # TODO: compute the baseline from T and use it to compute the depth map from
  # the disparity map
  depthMap=np.zeros_like(disp)
  for x in range(disp.shape[0]):
    for y in range(disp.shape[1]):
        depthMap[x,y]=focal_length[0]*np.linalg.norm(T)*1.0/(disp[x,y]+0.0001)#TODO check the disp range(added a small value so no division with 0)

  return depthMap
  
def write_ply_pcl(out_path, K, depth, im):
  w=im.shape[0]
  h=im.shape[1]
  log=open(out_path,'w')
  log.write("ply\n \bformat ascii 1.0\n element vertex "+str(w*h)+"\n property float x\n property float y\n property float z\nproperty uchar red\n"
                                                                  "property uchar green\n property uchar blue\n end_header\n")

  for i in range(w):
    for j in range(h):
      r=np.dot(np.linalg.inv(K),np.array([i, j, 1]))
      x=depth[i,j]*r
      log.write(str(x[0])+" "+str(x[1])+" "+str(x[2])+" "+str(im[i,j,0])+" "+str(im[i,j, ])+" "+str(im[i,j,2])+"\n")
  pass


def main():
  bin_paths = glob(os.path.join('../data', '*.bin'))
  bin_paths.sort()

  with open('../data/calib.json') as f:
    calib = json.load(f)['stereo_calibration_params']

  with open('../data/rect.json') as f:
    rect = json.load(f)['rect']

  for bin_path in bin_paths:
    if 'DEV1' in bin_path:
      cam_idx = 1 # left camera
    elif 'DEV2' in bin_path:
      cam_idx = 0 # right camera
    else:
      raise Exception('invalid camera/device')

    name, ext = os.path.splitext(bin_path)

    # preprocess images
    npy_path = '%s.npy' % name
    if os.path.exists(npy_path):
      im = np.load(npy_path)

    else:
      im = preprocess(bin_path)
      np.save(npy_path, im)


#     TODO: undistort images
    npy_path = '%s_undist.npy' % name
    if os.path.exists(npy_path):
      im_undist = np.load(npy_path)
    else:
      dist_coeffs = np.array(calib['refined_parameters'][cam_idx]['distortion']).ravel()
      focal_length = np.array(calib['refined_parameters'][cam_idx]['focal_length']).ravel()
      principal_point = np.array(calib['refined_parameters'][cam_idx]['principal_point']).ravel()
      im_size = (im.shape[1], im.shape[0])
    
      map = init_undistort(dist_coeffs, focal_length, principal_point, im_size)
      plt.figure("initial image")
      plt.imshow(im)
      im_undist = undistort(im, map)
      plt.figure("undistort image")
      plt.imshow(im_undist)
      plt.show()
      np.save(npy_path, im_undist)


      


    # TODO: rectify images
    npy_path = '%s_rect.npy' % name
    if not os.path.exists(npy_path):
      H = np.array( rect['rectifying_homography'][cam_idx] )
      im_rect = rectify(im_undist, H)
      plt.figure("im rect")
      plt.imshow(im_rect)
      plt.show()
      np.save(npy_path, im_rect)
      print(im_rect.shape)


  imr_paths = glob(os.path.join('../data', '*DEV2*rect.npy'))
  imr_paths.sort()
  iml_paths = glob(os.path.join('../data', '*DEV1*rect.npy'))
  iml_paths.sort()
  for imr_path, iml_path in zip(imr_paths, iml_paths):
    imr = np.load(imr_path).astype(np.float32)
    iml = np.load(iml_path).astype(np.float32)

    # TODO: compute disparity
    patch_size = 15
    step =10 # standard 4 but sloow

    dispr_path = '%s_disp.npy' % os.path.splitext(imr_path)[0]
    costr_path = '%s_cost.npy' % os.path.splitext(imr_path)[0]
    if os.path.exists(dispr_path) and os.path.exists(costr_path):
      dispr = np.load(dispr_path)
      costr = np.load(costr_path)
    else:
      tic = time.time()
      dispr, costr = pynative.disparity(imr, iml, patch_size=patch_size, step=step, dir=1)
      if step != 1:
        dispr = interpolate_nn(dispr, step, imr.shape)
        costr = interpolate_nn(costr, step, imr.shape)
      print('disparity computation took %f[s]' % (time.time() - tic))

      #np.save(dispr_path, dispr)
      #np.save(costr_path, costr)



    plt.figure("cost")
    plt.imshow(costr)
    plt.show()
    plt.figure("dispar")
    plt.imshow(dispr)
    plt.show()

    displ_path = '%s_disp.npy' % os.path.splitext(iml_path)[0]
    costl_path = '%s_cost.npy' % os.path.splitext(iml_path)[0]
    if os.path.exists(displ_path) and os.path.exists(costl_path):
      displ = np.load(displ_path)
      costl = np.load(costl_path)
    else:
      tic = time.time()
      displ, costl = pynative.disparity(iml, imr, patch_size=patch_size, step=step, dir=-1)
      if step != 1:
        displ = interpolate_nn(displ, step, iml.shape)
        costl = interpolate_nn(costl, step, iml.shape)
      print('disparity computation took %f[s]' % (time.time() - tic))

    #np.save(displ_path, dispr)
    #np.save(costl_path, costl)

      plt.figure("cost2")
      plt.imshow(costl)
      plt.show()
      plt.figure("disp2")
      plt.imshow(displ)
      plt.show()

    # TODO: postprocess disparity
    # dispr, costr = postprocess_disparity(dispr, displ, costr)

    # TODO: compute depth
    # T = calib['T']
    # focal_length = np.array(calib['refined_parameters'][0]['focal_length']).ravel()
    # depth = depth_from_disparity(dispr, T, focal_length)

    # TODO: write ply
    # K = np.array( rect['rectified_K'][0] )
    # write_ply_pcl('pcl.ply', K, depth, imr)


if __name__ == "__main__":
  main()
