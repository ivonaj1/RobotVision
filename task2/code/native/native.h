#ifndef NATIVE_H
#define NATIVE_H

void compute_disparity(const float* im1, const float* im2, int height, int width, int patch_size, int step, int dir, float* disp, float* cost);

#endif
