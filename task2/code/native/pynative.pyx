cimport cython
import numpy as np
cimport numpy as np

CREATE_INIT = True # workaround, so cython builds a init function

np.import_array() 


cdef extern from "native.h":
  void compute_disparity(const float* im1, const float* im2, int height, int width, int patch_size, int step, int dir, float* disp, float* cost);


def disparity(float[:,:,::1] im1, float[:,:,::1] im2, int patch_size, int step, int dir):
  cdef int height = im1.shape[0]
  cdef int width = im1.shape[1]
  if height != im2.shape[0] or width != im2.shape[1] or im1.shape[2] != im2.shape[2]:
    raise Exception('dimensions of im1 and im2 do not match')

  disp = np.zeros((height, width), dtype=np.float32)
  cdef float[:,::1] disp_view = disp
  cost = np.zeros((height, width), dtype=np.float32)
  cdef float[:,::1] cost_view = cost
  compute_disparity(&im1[0,0,0], &im2[0,0,0], height, width, patch_size, step, dir, &disp_view[0,0], &cost_view[0,0])

  return disp, cost
