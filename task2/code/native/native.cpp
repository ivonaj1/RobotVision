#include "native.h"
#include <cmath>
#include <cstdio>
#include <omp.h>


void compute_disparity(const float* im1, const float* im2, int height, int width, int patch_size, int step, int dir, float* disp, float* cost) {
  //TODO: compute the disparity map and associated matching costs using the 
  //color images im1 and im2 of size height x width x 3. 
  // patch_size is the size of the patches used for matching,
  // step is increment in height and width to compute a sparse disparity map
  //   and speed up the development process.
  // dir is the +1, or -1, depending on the search direction of the matching
  // patch in im2.
  int hidx = 0;
#pragma omp parallel for
  for(int h = 0; h < height; h += step) {//go through all horizontal lines
#pragma omp critical
    {
      printf("  h=%d/%d (step=%d,dir=%d)\n", hidx, height, step, dir);
      hidx += step;
    }
    int ps1,ps2;
    //TODO:
    int ul = 0; int ur = 0;float min,location1[3],location2[3],cc[3],ncc,stdev_l[3],stdev_r[3],pl,pr;
    for (int v_set = 0; v_set < width; v_set += step){//defining v for the right image
        min=100000.0;
        for (int v = v_set + dir*step; ((v < width) && v > 0); v += dir*step){
            location1[3]={0.0};
            location2[3]={0.0};
            cc[3]={0.0};
            ncc=0.0;
            stdev_l[3]={0.0};
            stdev_r[3]={0.0};
            pl=0.0;
            pr=0.0;

            for (int rgb=0; rgb<3; rgb++) {
                        ps1=0;
                        ps2=0;//number of pixels that exist in the patch
                        location1[rgb]=0;
                        location2[rgb]=0;

                for (int i = -patch_size/2; i < patch_size/2; i++) {

                    for (int j= -patch_size/2; j< patch_size/2; j++) {//calc centered patch1&2 locations on the horizontal line

                        if (!(i+h > height || v_set + j < 0 || v_set + j > width||i+h < 0)){
                            location1[rgb] += im1[(i+h) * width * 3 + (v_set + j) * 3 + rgb];
                            ps1++;
                            }
                        if (!(i+h > height || v + j < 0 || v + j > width||i+h < 0)){
                            location2[rgb] += im2[(i+h) * width * 3 + (v + j) * 3 + rgb];
                            ps2++;
                            }
                            }
                    }
                location1[rgb] /= pow(patch_size,2);
                location2[rgb] /=pow(patch_size,2);//means of patches

                //Matching-minimizer of the metric along horiz line
                int count1=0;
                int count2=0;
                for (int i = -patch_size/2; i < patch_size/2; i++) {
                    for (int j = -patch_size/2; j < patch_size/2; j++) {
                        pl=0;
                        pr=0;

                        if (!(i+h > height || v_set + j < 0 || v_set + j > width||i+h < 0)){
                          pl=im1[(i+h) * width * 3 + (v_set + j) * 3 + rgb];
                          count1++;}

                        if (!(i+h > height || v + j < 0 || v + j > width||i+h < 0)){
                           pr=im2[(i+h) * width * 3 + (v + j) * 3 + rgb];
                           count2++;}

                        cc[rgb] += ((pl - location1[rgb]) * (pr - location2[rgb]));//summing for normalized cross-correlation
                        stdev_l[rgb] +=pow((pl - location1[rgb]),2) ;
                        stdev_r[rgb] +=pow((pr - location2[rgb]),2);

                    }
                }
                //stdev_l[rgb] /=pow(count1,2);
                //stdev_r[rgb] /=pow(count2,2);
                ncc += (cc[rgb] / (sqrt(stdev_l[rgb]) * sqrt(stdev_r[rgb])));
            }
            ncc/=3.0;//mean of the channels
            if (ncc < min) {
                min = ncc;
                if(dir==1)
                    ul = v;
                    else
                    ur=v;
                }
            }
        cost[h*width + v_set] = min;
        if(dir==1)
            disp[h*width + v_set] = ul - v_set;
        else
            disp[h*width + v_set] = v_set - ur;
        }

    }
    }


