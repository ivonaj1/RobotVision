from distutils.core import setup
from Cython.Build import cythonize
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy as np
import platform

extra_compile_args = ['-msse', '-msse2', '-msse3', '-msse4.2']
extra_compile_args.append('-fopenmp')

extra_link_args = []
extra_link_args.append('-fopenmp')

setup(
  name="pynative",
  cmdclass= {'build_ext': build_ext},
  ext_modules=[
    Extension('pynative',
      ['pynative.pyx',
       'native.cpp',
       ],
      language='c++',
      include_dirs=[np.get_include()],
      extra_compile_args=extra_compile_args,
      extra_link_args=extra_link_args
    )
  ]
)
