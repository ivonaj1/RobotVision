import numpy as np
import cv2
import matplotlib.pyplot as plt
import random

def drawMatches(img1, kp1, img2, kp2, matches):


    rows1 = img1.shape[0]
    cols1 = img1.shape[1]
    rows2 = img2.shape[0]
    cols2 = img2.shape[1]
    out = np.zeros((max([rows1,rows2]),cols1+cols2,3), dtype='uint8')

    out[:rows1,:cols1,:] = np.dstack([img1, img1, img1])

    out[:rows2, cols1:] = np.dstack([img2, img2, img2])

    for mat in matches:
        img1_idx = mat.queryIdx
        img2_idx = mat.trainIdx

        (x1,y1) = kp1[img1_idx]
        (x2,y2) = kp2[img2_idx]

        cv2.circle(out, (int(x1),int(y1)), 10, (255, 0, 0), 2)
        cv2.circle(out, (int(x2)+cols1,int(y2)), 10, (255, 0, 0), 2)

        c = random.sample(range(256), 3)
        cv2.line(out, (int(x1),int(y1)), (int(x2)+cols1,int(y2)), c, 8)

    plt.figure()
    plt.imshow(out)
    plt.show()

    return out
def drawMatches2(img1, kp1, img2, kp2):


    rows1 = img1.shape[0]
    cols1 = img1.shape[1]
    rows2 = img2.shape[0]
    cols2 = img2.shape[1]
    out = np.zeros((max([rows1,rows2]),cols1+cols2,3), dtype='uint8')

    out[:rows1,:cols1,:] = np.dstack([img1, img1, img1])

    out[:rows2, cols1:] = np.dstack([img2, img2, img2])

    for i in range(kp1.shape[0]):

        (x1,y1) = kp1[i]
        (x2,y2) = kp2[i]

        cv2.circle(out, (int(x1),int(y1)), 10, (255, 0, 0), 2)
        cv2.circle(out, (int(x2)+cols1,int(y2)), 10, (255, 0, 0), 2)

        c = random.sample(range(256), 3)
        cv2.line(out, (int(x1),int(y1)), (int(x2)+cols1,int(y2)), c, 8)

    plt.figure()
    plt.imshow(out)
    plt.show()

    return out
