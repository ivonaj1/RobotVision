#!/usr/bin/env python

import numpy as np
import matplotlib

matplotlib.use('tkagg')
import matplotlib.pyplot as plt
import os
from glob import glob
import time
import pickle
import random
import config
import cv2
from drawmatches import drawMatches
from drawmatches import drawMatches2

from draw_inliers import drawInliers
from mpl_toolkits.mplot3d import Axes3D


def save_plt(path, remove_axis=False):
    """ Saves the current figure. """
    plt.tight_layout()
    if remove_axis:
        plt.axis('off')
    plt.savefig(path, dpi=300)


def kps2np(kps):
    kps_np = np.empty((len(kps), 2), dtype=np.float32)
    for row, kp in enumerate(kps):
        kps_np[row, 0] = kp.pt[0]
        kps_np[row, 1] = kp.pt[1]
    return kps_np


def dmatch2np(matches):
    matches_np = np.empty((len(matches), 2), dtype=np.int32)
    for row, match in enumerate(matches):
        matches_np[row, 0] = match.queryIdx
        matches_np[row, 1] = match.trainIdx
    return matches_np


def plt_matches(left_im, right_im, left_kp, right_kp, matches):
    # TODO implement the visualization of keypoint matches
    matches = sorted(matches, key=lambda val: val.distance)
    im = drawMatches(left_im, left_kp, right_im, right_kp, matches)


def rigid_motion(X, Y):
    # TODO compute the rigid motion R,t between the two point clouds
    # return R, T


    center_x = np.mean(X, axis=1)
    center_y = np.mean(Y, axis=1)
    # SVD with covariance matrix
    center_x = center_x.reshape(3, 1)  # vector
    center_y = center_y.reshape(3, 1)

    X_mean = np.repeat(center_x, X.shape[1], axis=1)  # (3*size)
    Y_mean = np.repeat(center_y, Y.shape[1], axis=1)

    cov = np.dot((Y - Y_mean), ((X - X_mean).T))  # covariance

    U, S, Vt = np.linalg.svd(cov)

    # rot and trasn part
    R = np.dot(Vt, U)
    T = center_x - R.dot(center_y)

    return R, T


def transform_points(pcl, R, T):
    # given pointcloud(3*6),rotation and translation matrix
    # RP+ T
    pcl_transf = np.zeros((3, pcl.shape[1]))
    transform = np.hstack((R, T))
    transform = np.vstack((transform, (np.array([0, 0, 0, 1]))))
    for i in range(pcl.shape[1]):
        col = pcl[:, i].reshape(3, 1)
        point = np.vstack((col, (np.array([1]))))
        point_trans = np.dot(transform, point)
        point_trans = point_trans / point_trans[3, :]
        pcl_transf[:, [i]] = point_trans[:3, :]

    return pcl_transf


def ransac(matches, kp0, kp1, X, Y, img1, img2, K, n, iters, min_dist):
    # TODO compute the rotation matrix R and and translation matrix using a


    inliers = []
    # RANSAC loop
    # first sample min number of data values
    for iter in range(iters):
        print('new iter\n')
        fwd_leng = matches.shape[0]
        match_id = random.sample(range(fwd_leng), n)

        # random(n) points
        X_rans = X[:, match_id]
        Y_rans = Y[:, match_id]

        # estimate transform with points
        R, T = rigid_motion(X_rans, Y_rans)  # R=3*3,T=3*1
        pcl = transform_points(Y, R, T)

        dist = np.linalg.norm((pcl - X), axis=0)
        curr_inls = np.where(dist < min_dist)
        curr_inls = curr_inls[0].tolist()

        if len(inliers) < len(curr_inls):
            inliers = curr_inls

    X_inliers = np.array(X[:, inliers])
    Y_inliers = np.array(Y[:, inliers])

    # drawInliers(img1, X_inliers, img2, Y_inliers)
    # inliers=[X_inliers,Y_inliers]
    drawMatches2(img1, kp0[inliers, :], img2, kp1[inliers, :])

    # compute R and t on all inlier points
    R, T = rigid_motion(X_inliers, Y_inliers)

    return R, T, X_inliers, Y_inliers


def depth_from_disparity(matches, T, K, kp_left, kp_right):
    # TODO: compute the baseline from T and use it to compute the depth map from
    # variables named according to task2.6.

    focal_length = K[0][0]
    depthMap = np.zeros((3, len(matches)))

    for i, match in enumerate(matches):
        uv = np.array([kp_left[match.queryIdx, 0], kp_left[match.queryIdx, 1], 1])
        dist = kp_left[match.queryIdx, 0] - kp_right[match.trainIdx, 0]
        z = focal_length * np.sqrt(np.square(T[0]) + np.square(T[1]) + np.square(T[2])) / (dist)  # z = f*b / (xl - xr)
        r = np.linalg.inv(K).dot(uv ) # viewing ray
        depthMap[:, i] = r*z

    return depthMap


def main():
    data_paths = sorted(glob(os.path.join('../data', '*.npz')))

    with open('../data/calib.pkl', 'rb') as f:
        data = pickle.load(f, encoding='latin1')
        K = data['K']
        T = data['T']

    # TODO compute keypoints and features
    # sift = cv2.xfeatures2d.SIFT_create(nfeatures=20000, contrastThreshold=0.0001)
    sift = cv2.xfeatures2d.SIFT_create(nfeatures=20000, contrastThreshold=0.0001)
    for idx, data_path in enumerate(data_paths):
        features_path = os.path.join('../data', 'features%04d.pkl' % idx)
        if not os.path.exists(features_path):
            data = np.load(data_path)
            im_left = data['gray_left']
            im_right = data['gray_right']

            print('compute descriptors %d/%d' % (idx + 1, len(data_paths)))
            tic = time.time()
            # TODO compute SIFT image keypoints and descriptors for left and right
            # take indices of highlights (everything thats nearly white)
            highlights = np.where(im_left > 250)
            highlights_array = np.zeros((highlights[0].size, 2))  # array instead of tuple for easier access
            highlights_array[:, 0] = highlights[0].astype(int)
            highlights_array[:, 1] = highlights[1].astype(int)

            mask_left = np.ones_like(im_left)  # create mask for left and right image
            mask_right = np.ones_like(im_right)  # 1 = relevant 0 = ignored

            for x, y in highlights_array.astype(int):  # for every highlight pixel
                mask_left[x - 5:x + 5, y - 5:y + 5] = 0  # mark neighbourhood as highlight

            highlights = np.where(im_right > 250)  # hightlights for right image
            highlights_array = np.zeros((highlights[0].size, 2))
            highlights_array[:, 0] = highlights[0].astype(int)
            highlights_array[:, 1] = highlights[1].astype(int)

            for x, y in highlights_array.astype(int):  # for every highlight pixel
                mask_right[x - 5:x + 5, y - 5:y + 5] = 0  # mark neighbourhood as highlight
            print('  took %f[s]' % (time.time() - tic))

            kp_left, des_left = sift.detectAndCompute(im_left, mask_left)  # get keypoints & descriptors from images
            kp_right, des_right = sift.detectAndCompute(im_right, mask_right)

            bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True)  # init cc matcher
            matcher = bf.match(des_left, des_right)

            kp_right = kps2np(kp_right)
            kp_left = kps2np(kp_left)

            plt_matches(im_left, im_right, kp_left, kp_right, matcher)
            with open(features_path, 'wb') as f:
                pickle.dump({'kp_right': kp_right, 'kp_left': kp_left, 'des_right': des_right, 'des_left': des_left}, f)


    # sparse stereo feature matching and triangulation
    features_paths = sorted(glob(os.path.join('../data/', 'features*pkl')))
    for idx in range(len(features_paths)):
        stereo_matches_path = os.path.join('../data', 'stereo_matches%04d.pkl' % idx)
        if not os.path.exists(stereo_matches_path):
            with open(features_paths[idx], 'rb') as f:
                data = pickle.load(f, encoding='latin1')
                kp_left = data['kp_left']
                kp_right = data['kp_right']
                des_left = data['des_left']
                des_right = data['des_right']

                im_left = np.load(data_paths[idx])['gray_left']
                im_right = np.load(data_paths[idx])['gray_right']

            print('match descriptors stereo')
            tic = time.time()
            # TODO compute the descriptor matches using cross checking
            print('  took %f[s]' % (time.time() - tic))
            bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True)  # init cc matcher
            matches = bf.match(des_left, des_right)
            matches = sorted(matches, key=lambda x: x.distance)
            matches2np = dmatch2np(matches)  # change to numpy

            # |kp_left.y - kp_right.y| < t: t in [1,4] and disp>0

            index = np.where(np.logical_and((np.abs(kp_left[matches2np[:, 0], 1] - kp_right[matches2np[:, 1], 1]) < 3),
            (kp_left[matches2np[:, 0], 0] - kp_right[matches2np[:, 1], 0]) > 0))  # removes diagonal lines afterwards
            matches4depth = [matches[i] for i in index[0].tolist()]

            index_left = []
            for mat in matches4depth:  # use only the indexes of the filtered matches
                index_left.append(mat.queryIdx)


            # TODO compute the sparse 3D point cloud xyz using the matches
            plt_matches(im_left, im_right, kp_left, kp_right, matches4depth)
            xyz = depth_from_disparity(matches4depth, T, K, kp_left,kp_right)  # calculate depth - something is maybe wrong in the function

            with open(stereo_matches_path, 'wb') as f:
                pickle.dump({'kp': kp_left[np.asarray(index_left), :], 'des': des_left[np.asarray(index_left), :], 'xyz': xyz}, f)

    # forward matching
    stereo_matches_paths = sorted(glob(os.path.join('../data/', 'stereo_matches*pkl')))
    for idx in range(len(features_paths) - 1):
        fwd_matches_path = os.path.join('../data', 'fwd_matches%04d.pkl' % idx)
        if not os.path.exists(fwd_matches_path):
            with open(stereo_matches_paths[idx], 'rb') as f:
                data = pickle.load(f, encoding='latin1')
                kp0 = data['kp']
                des0 = data['des']
            with open(stereo_matches_paths[idx + 1], 'rb') as f:
                data = pickle.load(f, encoding='latin1')
                kp1 = data['kp']
                des1 = data['des']

            im1 = np.load(data_paths[idx])['gray_left']
            im2 = np.load(data_paths[idx + 1])['gray_left']
            print('match descriptors fwd')
            tic = time.time()
            # TODO match the descriptors of two consecutive images usig a ratio test
            print('  took %f[s]' % (time.time() - tic))

            # ratio test from Lowe's SIFT paper... compute (score of the best feature match)/(score of the second best feature match)
            bf = cv2.BFMatcher()
            matches = bf.knnMatch(des0, des1, k=2)

            fwd_matches = []
            for m, n in matches:
                if m.distance < 0.75 * n.distance:
                    fwd_matches.append(m)

            plt_matches(im1, im2, kp0, kp1, fwd_matches)

            with open(fwd_matches_path, 'wb') as f:
                pickle.dump({'matches': dmatch2np(fwd_matches)}, f)

    R_total = np.eye(3, 3)  # init matrices for transform to the first frame
    T_total = np.zeros((3, 1))
    fwd_matches_paths = sorted(glob(os.path.join('../data/', 'fwd_matches*pkl')))
    for idx in range(len(features_paths) - 1):
        trans_path = os.path.join('../data', 'trans%04d.pkl' % idx)
        if not os.path.exists(trans_path):
            with open(stereo_matches_paths[idx], 'rb') as f:
                data = pickle.load(f, encoding='latin1')
                kp0 = data['kp']
                des0 = data['des']
                xyz0 = data['xyz']
            with open(stereo_matches_paths[idx + 1], 'rb') as f:
                data = pickle.load(f, encoding='latin1')
                kp1 = data['kp']
                des1 = data['des']
                xyz1 = data['xyz']
            with open(fwd_matches_paths[idx], 'rb') as f:
                matches = pickle.load(f, encoding='latin1')['matches']

            img1 = np.load(data_paths[idx])['gray_left']
            img2 = np.load(data_paths[idx + 1])['gray_left']

            # Read fwd matches

            id0 = matches[:, 0]
            id1 = matches[:, 1]
            kp0 = kp0[id0, :]
            kp1 = kp1[id1, :]
            xyz0 = xyz0[:, id0]
            xyz1 = xyz1[:, id1]

            # TODO: compute R and t using the least-squares solution in a RANSAC loop
            R, T, X_inliers, Y_inliers = ransac(matches, kp0, kp1, xyz0, xyz1, img1, img2, K, n=6, iters=1500,min_dist=7)

            # drawInliers(img1, X_inliers, img2, Y_inliers)
            inliers = [X_inliers, Y_inliers]
            R_total = R.dot(R_total)  # transform to the first frame
            T_total = R.dot(T_total) + T
            with open(trans_path, 'wb') as f:
                pickle.dump({'R': R, 'T': T, 'inliers': inliers}, f)

    # transform points & write ply file
    trans_paths = sorted(glob(os.path.join('../data/', 'trans*pkl')))

    for idx, data_path in enumerate(data_paths):
        print('write ply file %d/%d' % (idx + 1, len(data_paths)))

        data = np.load(data_path)
        roi = config.roi_left_xywh
        rgb = data['color_left'][roi[1]:roi[1] + roi[3], roi[0]:roi[0] + roi[2], :]
        xyz = data['depth']

        if idx == 0:  # first image should not be transformed
            xyz_trans = xyz[xyz[:, :, 2] > 0].T  # only those points with a correct depth are used
            rgb_trans = rgb[xyz[:, :, 2] > 0].T  # same for rgb values


        else:  # the next image images are transformed
            with open(trans_paths[idx - 1], 'rb') as f:
                data = pickle.load(f)
                Rn = data['R']
                Tn = data['T']

            # TODO: write the colored ply file
            # NOTE: you can write one ply file per point cloud
            xyz_trans = xyz[xyz[:, :, 2] > 0].T  # only those points with a correct depth are used
            rgb_trans = rgb[xyz[:, :, 2] > 0].T  # same for rgb values

            xyz_trans = Rn.dot(xyz_trans) + Tn  # transform them into the first image

        f = open("mesh_" + str(idx) + ".ply", 'w')
        f.write('ply\n')
        f.write('format ascii 1.0\n')
        f.write('element vertex %s\n' % int(xyz_trans.shape[1]))
        f.write('property float x\n')
        f.write('property float y\n')
        f.write('property float z\n')
        f.write('property uchar red\n')
        f.write('property uchar green\n')
        f.write('property uchar blue\n')
        f.write('end_header\n')

        for i in range(xyz_trans.shape[1]):  # number of points
            f.write("%f %f %f %s %s %s\n" % (
                float(xyz_trans[0, i]),
                float(xyz_trans[1, i]),
                float(xyz_trans[2, i]),
                int(rgb_trans[0, i] * 255),
                int(rgb_trans[1, i] * 255),
                int(rgb_trans[2, i] * 255)))

        f.close()


if __name__ == "__main__":
    main()
